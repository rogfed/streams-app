import {
  FETCH_STREAM,
  FETCH_STREAMS,
  CREATE_STREAM,
  EDIT_STREAM,
  DELETE_STREAM,
} from '../actions/types';
import omit from 'lodash/omit';
import mapKeys from 'lodash/mapKeys';

const streamsReducer = (state = {}, { type, payload }) => {
  switch (type) {
    case FETCH_STREAM:
    case CREATE_STREAM:
    case EDIT_STREAM:
      return { ...state, [payload.id]: payload };
    case DELETE_STREAM:
      return omit(state, payload);
    case FETCH_STREAMS:
      return { ...mapKeys(payload, 'id') };
    default:
      return state;
  }
};

export default streamsReducer;
