import React from 'react';
import { Form, Field } from 'react-final-form';
import PropTypes from 'prop-types';

const StreamForm = ({ onSubmit, initialValues }) => {
  const renderError = ({ error, touched }) => {
    if (touched && error) {
      return (
        <div className='ui error message'>
          <div className='header'>{error}</div>
        </div>
      );
    }
  };

  const renderInput = ({ input, label, meta }) => {
    const className = `field ${meta.error && meta.touched ? 'error' : ''}`;
    return (
      <div className={className}>
        <label>{label}</label>
        <input {...input} autoComplete='off' />
        {renderError(meta)}
      </div>
    );
  };

  const triggerSubmit = formValues => {
    onSubmit(formValues);
  };

  return (
    <Form
      initialValues={initialValues}
      onSubmit={triggerSubmit}
      validate={formValues => {
        const errors = {};

        if (!formValues.title) {
          errors.title = 'You must enter a title';
        }

        if (!formValues.description) {
          errors.description = 'You must enter a description';
        }

        return errors;
      }}
      render={({ handleSubmit }) => (
        <form onSubmit={handleSubmit} className='ui form error'>
          <Field name='title' component={renderInput} label='Enter Title' />
          <Field
            name='description'
            component={renderInput}
            label='Enter Description'
          />
          <button className='ui button primary'>Submit</button>
        </form>
      )}
    />
  );
};

StreamForm.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  initialValues: PropTypes.object,
};

export default StreamForm;
