import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import pick from 'lodash/pick';
import PropTypes from 'prop-types';

import { fetchStream, editStream } from '../../actions';
import StreamForm from './StreamForm';

const StreamEdit = ({
  fetchStream,
  match: {
    params: { id },
  },
  editStream,
  stream,
}) => {
  useEffect(() => {
    fetchStream(id);
  }, []);

  const onSubmit = formValues => {
    editStream(id, formValues);
  };

  if (!stream) return null;

  return (
    <div>
      <h3>Edit Stream</h3>
      <StreamForm
        onSubmit={onSubmit}
        initialValues={pick(stream, 'title', 'description')}
      />
    </div>
  );
};

const mapStateToProps = (
  { streams },
  {
    match: {
      params: { id },
    },
  }
) => {
  return { stream: streams[id] };
};

StreamEdit.propTypes = {
  fetchStream: PropTypes.func.isRequired,
  match: PropTypes.object.isRequired,
  editStream: PropTypes.func.isRequired,
  stream: PropTypes.object.isRequired,
};

export default connect(mapStateToProps, { fetchStream, editStream })(
  StreamEdit
);
