import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

import Modal from '../Modal';
import history from '../../history';
import { fetchStream, deleteStream } from '../../actions';

const StreamDelete = ({
  match: {
    params: { id },
  },
  fetchStream,
  deleteStream,
  stream,
}) => {
  useEffect(() => {
    fetchStream(id);
  }, []);

  const renderActions = () => (
    <>
      <div className='ui button negative' onClick={() => deleteStream(id)}>
        Delete
      </div>
      <Link className='ui button' to='/'>
        Cancel
      </Link>
    </>
  );

  const renderContent = () =>
    stream
      ? `Are you sure you want to delete the stream with title: ${stream.title}`
      : 'Are you sure you want to delete this stream?';

  return (
    <Modal
      title='Delete Stream'
      content={renderContent()}
      actions={renderActions()}
      onDismiss={() => {
        history.push('/');
      }}
    />
  );
};

const mapStateToProps = (
  { streams },
  {
    match: {
      params: { id },
    },
  }
) => ({
  stream: streams[id],
});

StreamDelete.propTypes = {
  match: PropTypes.object.isRequired,
  fetchStream: PropTypes.func.isRequired,
  deleteStream: PropTypes.func.isRequired,
  stream: PropTypes.object.isRequired,
};

export default connect(mapStateToProps, { fetchStream, deleteStream })(
  StreamDelete
);
