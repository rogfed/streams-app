import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

import { fetchStreams } from '../../actions';

const StreamList = ({ fetchStreams, streams, currentUserId, isSignedIn }) => {
  useEffect(() => {
    fetchStreams();
  }, []);

  const renderList = () => {
    return streams.map(({ id, title, description, userId }) => (
      <div className='item' key={id}>
        {renderAdmin(userId, id)}
        <i className='large middle aligned icon camera' />
        <div className='content'>
          <Link to={`/streams/${id}`}>{title}</Link>
          <div className='description'>{description}</div>
        </div>
      </div>
    ));
  };

  const renderAdmin = (userId, id) => {
    if (userId === currentUserId)
      return (
        <div className='right floated content'>
          <Link className='ui button primary' to={`/streams/edit/${id}`}>
            Edit
          </Link>
          <Link className='ui button negative' to={`/streams/delete/${id}`}>
            Delete
          </Link>
        </div>
      );
  };

  const renderCreate = () => {
    return (
      isSignedIn && (
        <div style={{ textAlign: 'right' }}>
          <Link to='/streams/new' className='ui button primary'>
            Create Stream
          </Link>
        </div>
      )
    );
  };

  return (
    <div>
      <h2>Streams</h2>
      <div className='ui celled list'>{renderList()}</div>
      {renderCreate()}
    </div>
  );
};

const mapStateToProps = ({ streams, auth }) => {
  const { userId: currentUserId, isSignedIn } = auth;
  return {
    streams: Object.values(streams),
    currentUserId,
    isSignedIn,
  };
};

StreamList.propTypes = {
  fetchStreams: PropTypes.func.isRequired,
  streams: PropTypes.array.isRequired,
  currentUserId: PropTypes.string,
  isSignedIn: PropTypes.bool,
};

export default connect(mapStateToProps, {
  fetchStreams,
})(StreamList);
