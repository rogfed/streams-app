import React, { useRef, useEffect } from 'react';
import { connect } from 'react-redux';
import flv from 'flv.js';
import PropTypes from 'prop-types';

import { fetchStream } from '../../actions';

const StreamShow = ({
  fetchStream,
  match: {
    params: { id },
  },
  stream,
}) => {
  const videoRef = useRef(null);
  let player;

  useEffect(() => {
    fetchStream(id);

    return () => {
      player.destroy();
    };
  }, []);

  useEffect(() => {
    buildPlayer();
  });

  const buildPlayer = () => {
    if (player || !stream) return;

    player = flv.createPlayer({
      type: 'flv',
      url: `http://localhost:8000/live/${id}.flv`,
    });
    player.attachMediaElement(videoRef.current);
    player.load();
  };

  return stream ? (
    <div>
      <video ref={videoRef} style={{ width: '100%' }} controls />
      <h1>{stream.title}</h1>
      <h5>{stream.description}</h5>
    </div>
  ) : (
    <div>Loading...</div>
  );
};

const mapStateToProps = (
  { streams },
  {
    match: {
      params: { id },
    },
  }
) => ({
  stream: streams[id],
});

StreamShow.propTypes = {
  fetchStream: PropTypes.func.isRequired,
  match: PropTypes.object.isRequired,
  stream: PropTypes.object.isRequired,
};

export default connect(mapStateToProps, { fetchStream })(StreamShow);
