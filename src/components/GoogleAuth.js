import React, { useEffect, useRef } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { signIn, signOut } from '../actions';

const GoogleAuth = ({ signIn, signOut, isSignedIn }) => {
  const auth = useRef();

  useEffect(() => {
    window.gapi.load('client:auth2', () => {
      window.gapi.client
        .init({
          clientId:
            '557335638673-laem37pe67rf32lue4affs6bbhbgietg.apps.googleusercontent.com',
          scope: 'email',
        })
        .then(() => {
          auth.current = window.gapi.auth2.getAuthInstance();
          onAuthChange(auth.current.isSignedIn.get());
          auth.current.isSignedIn.listen(onAuthChange);
        });
    });
  }, []);

  const onAuthChange = isSignedIn =>
    isSignedIn ? signIn(auth.current.currentUser.get().getId()) : signOut();

  const onSignInClick = () => auth.current.signIn();

  const onSignOutClick = () => auth.current.signOut();

  const renderAuthButton = () =>
    isSignedIn === null ? (
      <div>I don't know if we are signed in</div>
    ) : isSignedIn ? (
      <button className='ui red google button' onClick={onSignOutClick}>
        <i className='google icon' />
        Sign Out
      </button>
    ) : (
      <button className='ui red google button' onClick={onSignInClick}>
        <i className='google icon' />
        Sign In with Google
      </button>
    );

  return <div>{renderAuthButton()}</div>;
};

const mapStateToProps = ({ auth }) => ({
  isSignedIn: auth.isSignedIn,
});

GoogleAuth.propTypes = {
  signIn: PropTypes.func.isRequired,
  signOut: PropTypes.func.isRequired,
  isSignedIn: PropTypes.bool,
};

export default connect(mapStateToProps, {
  signIn,
  signOut,
})(GoogleAuth);
